#!/bin/bash

# This script compiles typescript and Angular application and puts them into a single NodeJS prject
echo "-- Started build script for Angular & NodeJS --"
echo "Removing dist directory..."
rm -rf dist

echo "Compiling typescript..."
npm run build:backend

echo "Installing Angular app dependencies..."
cd src/client && npm install

echo "Building Angular app for distribution..."
npm run build


echo "Copying angular dist into dist directory..."
mkdir ../../dist/static
# cp -Rf dist ../../dist/static
cp -a dist/browser/. ../../dist/static

echo "Removing angular-src dist directory..."
# rm -rf dist

# Go back to the current directory
cd ../..

echo "-- Finished building Angular & NodeJS, check out directory --"
