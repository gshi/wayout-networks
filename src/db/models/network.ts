import { Typegoose, prop } from 'typegoose';
import { INetwork } from '../../shared/interfaces';
import * as uuidv4 from 'uuid/v4';

function _getCurrentTimeStamp() {
    return new Date().getTime() / 1000
}

export class Network extends Typegoose implements INetwork {

    @prop({default: uuidv4.default, unique: true, required: true})
    id: string;

    @prop({required: true})
    name: string;

    @prop({required: true, default: _getCurrentTimeStamp})
    timeCreated: number;

    @prop({required: true})
    deviceType: string;

    @prop()
    lastSuccessfulConn: number;

}

export const NetworkModel = new Network().getModelForClass(Network);

