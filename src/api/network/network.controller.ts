import { INetwork } from '../../shared/interfaces';
import { Model } from 'mongoose';

export class NetworkController {
    constructor(private networkModel: Model<any>) {}

    get() {
        return this.networkModel.find();
    }

    create(network: INetwork) {
        return this.networkModel.create(network);
    }

    update(networkId: string, network: INetwork) {
        return this.networkModel.update({id: networkId}, network);
    }

    delete(networkId: string) {
        return this.networkModel.deleteOne({id: networkId});
    }
}