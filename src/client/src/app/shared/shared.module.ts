import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { AddItemComponent } from './components/add-item/add-item.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule
  ],
  declarations: [AddItemComponent],
  exports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    AddItemComponent,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class SharedModule { }
