
import { NetworkActions, NetworkActionTypes } from '../actions/network.actions';
import { NetworkApiActions, NetworkApiActionTypes } from '../actions/network-api.actions';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { INetwork } from '../../../../../shared/interfaces';
import * as fromRoot from '../../reducers';
import { createSelector } from '@ngrx/store';

interface NetworkState extends EntityState<INetwork> {
  selectedNetworkId: string;
}

export interface State extends fromRoot.State {
  network: NetworkState;
}

export const adapter: EntityAdapter<INetwork> = createEntityAdapter<INetwork>({
  selectId: (network: INetwork) => network.id,
  sortComparer: false,
});

export const initialState: NetworkState = adapter.getInitialState({
  selectedNetworkId: null
})

export function reducer(state = initialState, action: NetworkActions | NetworkApiActions): NetworkState {
  switch (action.type) {

    case NetworkActionTypes.LoadNetworks:
      return state;

    case NetworkApiActionTypes.CreateNetworkSuccess:
      return adapter.addOne(action.payload, state);

    case NetworkApiActionTypes.LoadNetworksSuccess:
      return adapter.addMany(action.payload, state);

    case NetworkApiActionTypes.UpdateNetworkSuccess:
      return adapter.updateOne(action.payload, state);

    case NetworkApiActionTypes.DeleteNetworkSuccess:
     return adapter.removeOne(action.payload, state);

    case NetworkActionTypes.SelectNetwork:
      return {
        ...state,
        selectedNetworkId: action.payload
      };

    case NetworkActionTypes.DeselectNetwork:
      return {
        ...state,
        selectedNetworkId: null
      }

    default:
      return state;
  }
}

export const getNetworksState = (state: State) => state.network;

export const getAllNetworks = adapter.getSelectors(getNetworksState).selectAll;

export const getSelectedNetworkId = createSelector(getNetworksState, (s) => s.selectedNetworkId);
