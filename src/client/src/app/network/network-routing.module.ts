import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NetworksPageComponent } from './containers/networks-page/networks-page.component';

const routes: Routes = [
  {
    path: '',
    component: NetworksPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NetworkRoutingModule { }
