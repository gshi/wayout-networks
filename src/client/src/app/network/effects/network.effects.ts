import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';

import { concatMap, map, catchError, switchMap } from 'rxjs/operators';
import { EMPTY, of } from 'rxjs';
import { NetworkActionTypes, NetworkActions, CreateNetwork } from '../actions/network.actions';
import {
  CreateNetworkSuccess,
  CreateNetworkFailed,
  LoadNetworksSuccess,
  LoadNetworksFailed,
  UpdateNetworkSuccess,
  UpdateNetworkFailed,
  DeleteNetworkSuccess,
  DeleteNetworkFailed
} from '../actions/network-api.actions';
import { NetworkService } from '../services';
import { ToastService } from 'src/app/core/services';


@Injectable()
export class NetworkEffects {


  @Effect()
  loadNetworks$ = this.actions$.pipe(
    ofType(NetworkActionTypes.LoadNetworks),
    concatMap(() => this.networkService.getNetworks().pipe(
      map(networks => new LoadNetworksSuccess(networks)),
      catchError(err => {
        this.toastService.showToast('Error fetching networks from server');
        return of(new LoadNetworksFailed());
      })
    ))
  );


  @Effect()
  createNetwork$ = this.actions$.pipe(
    ofType(NetworkActionTypes.CreateNetwork),
    switchMap((action: any) => this.networkService.createNetwork(action.payload)
      .pipe(
        map((network) => {
          this.toastService.showToast('Network created');
          return new CreateNetworkSuccess(network);
        }),
        catchError(err => {
          this.toastService.showToast('Error creating network');
          return of(new CreateNetworkFailed());
        })
      )
    )
  );

  @Effect()
  editNetwork$ = this.actions$.pipe(
    ofType(NetworkActionTypes.UpdateNetwork),
    switchMap((action: any) => this.networkService.updateNetwork(action.payload)
      .pipe(
        map((network) => {
          this.toastService.showToast('Network updated');
          return new UpdateNetworkSuccess(action.payload);
        }),
        catchError(err => {
          this.toastService.showToast('Error updating network');
          return of(new UpdateNetworkFailed());
        })
      )
    )
  );

  @Effect()
  deleteNetwork$ = this.actions$.pipe(
    ofType(NetworkActionTypes.DeleteNetwork),
    switchMap((action: any) => this.networkService.deleteNetwork(action.payload)
      .pipe(
        map((network) => {
          this.toastService.showToast('Network deleted');
          return new DeleteNetworkSuccess(action.payload);
        }),
        catchError(err => {
          this.toastService.showToast('Error deleting network');
          return of(new DeleteNetworkFailed());
        })
      )
    )
  );


  constructor(
    private actions$: Actions<NetworkActions>,
    private networkService: NetworkService,
    private toastService: ToastService
  ) { }

}
