import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/core/services';
import { Observable, of } from 'rxjs';
import { INetwork } from '../../../../../shared/interfaces';
import { Update } from '@ngrx/entity';

@Injectable()
export class NetworkService {

    constructor(private apiService: ApiService) {}


    private _getNetworkEndpoint(path?: string): string {
        return path ? `network/${path}` : 'network';
    }

    getNetworks(): Observable<INetwork[]> {
        return this.apiService.get(this._getNetworkEndpoint());
    }

    createNetwork(network: INetwork): Observable<INetwork> {
        return this.apiService.post(this._getNetworkEndpoint(), network);
    }

    updateNetwork(networkUpdate: Update<INetwork>): Observable<INetwork> {
        return this.apiService.put(this._getNetworkEndpoint(networkUpdate.id as string), networkUpdate.changes);
    }

    deleteNetwork(networkId: string): Observable<void> {
        return this.apiService.delete(this._getNetworkEndpoint(networkId));
    }
}