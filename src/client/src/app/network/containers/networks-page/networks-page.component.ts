import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { EditCreateNetworkComponent } from '../edit-create-network/edit-create-network.component';
import { Store, select } from '@ngrx/store';
import * as fromNetwork from '../../reducers/network.reducer';
import { LoadNetworks, SelectNetwork, DeselectNetwork, DeleteNetwork } from '../../actions/network.actions';
import { INetwork } from '../../../../../../shared/interfaces';

@Component({
  selector: 'app-networks-page',
  templateUrl: './networks-page.component.html',
  styleUrls: ['./networks-page.component.css']
})
export class NetworksPageComponent implements OnInit {

  constructor(
    private dialog: MatDialog,
    private store: Store<fromNetwork.State>
  ) { }

  networks$ = this.store.pipe(select(fromNetwork.getAllNetworks));
  selectedNetworkId$ = this.store.pipe(select(fromNetwork.getSelectedNetworkId))

  ngOnInit() {
    this.store.dispatch(new LoadNetworks());
  }

  openNetworkDialog() {
    this.dialog.open(EditCreateNetworkComponent, { width: '320px' });
  }

  onNetworkSelect(networkId: string) {
    this.store.dispatch(new SelectNetwork(networkId));
  }

  onNetworkDeselect() {
    this.store.dispatch(new DeselectNetwork());
  }

  openEditNetworkDialog(network: INetwork) {
    this.dialog.open(EditCreateNetworkComponent, {width: '320px', data: network})
  }

  deleteNetwork(networkId: string) {
    this.store.dispatch(new DeleteNetwork(networkId));
  }

}
