import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { INetwork } from '../../../../../../shared/interfaces';
import { MatSnackBar } from '@angular/material/snack-bar';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-network',
  templateUrl: './network.component.html',
  styleUrls: ['./network.component.css'],
  animations: [
    trigger('rotate', [
      state('close', style({transform: '*'})),
      state('open', style({transform: 'rotate(90deg)'})),
      transition('open <=> close', [
        animate('0.2s')
      ])
    ]),
    trigger('fade', [
      transition(':enter', [
        style({height: '0px', opacity: '0'}),
        animate('0.2s', style({height: '*'})),
        animate('0.2s', style({opacity: 1}))
      ]),
      transition(':leave', [
        style({height: '*', opacity: '*'}),
        animate('0.2s', style({opacity: '0'})),
        animate('0.2s', style({height: '0px'}))
      ]),
    ])
  ]
})
export class NetworkComponent implements OnInit {

  constructor() { }

  @Input()
  network: INetwork;

  @Input()
  isExpended = false;

  @Output()
  selected = new EventEmitter<any>();

  @Output()
  deselected = new EventEmitter<any>();

  @Output()
  editClicked = new EventEmitter<any>();

  @Output()
  deleteClicked = new EventEmitter<any>();

  ngOnInit() {
  }

  onClick() {
    if (this.isExpended) {
      this.deselected.emit();
    } else {
      this.selected.emit();
    }
  }

  onEditClick() {
    this.editClicked.emit();
  }

  onDeleteClick() {
    this.deleteClicked.emit();
  }

}
