import { NgModule } from '@angular/core';
import { Routes, RouterModule, Route } from '@angular/router';
import { TestComponent } from './test/test.component';

const routes: Route[] = [
    // {
      // path: '',
      // pathMatch: 'full',
      // component: HomeComponent
    // },
    {
      path: 'test',
      pathMatch: 'full',
      component: TestComponent
    },
    {
        path: 'networks',
        loadChildren: './network/network.module#NetworkModule'
    }
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
