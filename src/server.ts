import path from 'path';
process.env['NODE_CONFIG_DIR'] = path.join(__dirname, 'config');
import config from 'config';
import {logger} from './logger';
logger.info('hello', {actionsId: 'hi'});

import express from 'express';
import httpErrors from 'http-errors';
import cors from 'cors';
import bodyParser from 'body-parser';
import {connectToDb} from './db';
import apiRouter from './api';


connectToDb();

const app = express();

// TODO: add cors options
app.use(cors());

app.use(bodyParser.json());


app.use('/api', apiRouter);

app.use(express.static(path.join(__dirname, 'static')));

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'static/index.html'));
});

app.use((req, res, next) => {
    next(new httpErrors.NotFound());
});

app.use((err: any | Error, req: express.Request, res: express.Response, next: express.NextFunction) => {
    console.log(err);
    let message;
    if (err instanceof httpErrors.HttpError) {
      logger.warn(err);
      message = err.message;
    } else {
      logger.error(err.stack)
      message = 'הירעה שגיאה';
    }
    res.status(err.status || 500);
    res.send(message);
  });

app.listen(config.get('listenOnPort'), () => logger.info(`listening on port ${config.get('listenOnPort')}`));
