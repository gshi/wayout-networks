// const winston = require('winston');
import * as winston from 'winston'
import {format} from 'winston';


const alignedWithColorsAndTime = format.combine(
    format.timestamp(),
    format.printf(info => `${info.timestamp} ${info.level}: ${info.actionId}// ${info.message}`)
  );

export const logger = winston.createLogger({
    level: 'info',
    format: alignedWithColorsAndTime,
    transports: [
      new winston.transports.File({ filename: './logs/error.log', level: 'error' }),
      new winston.transports.File({ filename: './logs/action.log' })
    ]
  });